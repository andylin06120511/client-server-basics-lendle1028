/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.clientserverbasics;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author lendle
 */
public class TimeClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        //hint: 建立 Socket 物件，並且取得 input stream 儲存在 input 變數中
        Socket socket=null;
        InputStream input=null;
        socket=new Socket("time-a.nist.gov", 13);
        input=socket.getInputStream();
        /////////////////////////////////////////////////////////////
        StringBuilder sb =new StringBuilder();
        while(true){
            int b=input.read();
            if(b==-1){
            break;
            }
            char c=(char)b;
            sb.append(c);
        }
        System.out.println(sb);
        socket.close();
    }
    
}
